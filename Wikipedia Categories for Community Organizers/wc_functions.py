import warnings
import pandas as pd
import json
import subprocess
import requests
import io
import matplotlib.pyplot as plt
from qwikidata.sparql import return_sparql_query_results
import importlib
import subprocess

def install_module(module_name):
    print(f"Module '{module_name}' not found. Installing it now...")
    subprocess.call(['pip', 'install', module_name])

# Check and install 'wikipedia'
try:
    importlib.import_module('wikipedia')
except ImportError:
    install_module('wikipedia')
finally:
    import wikipedia  # Now it should be imported

# Check and install 'wordcloud'
try:
    importlib.import_module('wordcloud')
except ImportError:
    install_module('wordcloud')
finally:
    from wordcloud import WordCloud  # Now it should be imported

# Check and install 'qwikidata'
try:
    importlib.import_module('qwikidata.sparql')
except ImportError:
    install_module('qwikidata')
finally:
    from qwikidata.sparql import return_sparql_query_results 


def transform_url(original_url):
    # Extract the course name from the original URL
    course_name = original_url.split('/courses/')[-1]
    
    # Create the target URL
    target_url = f"https://outreachdashboard.wmflabs.org/course_edits_csv?course={course_name}"
    
    return target_url

def create_dataframe(given_url):
    url = given_url
    # Send a GET request to download the file
    response = requests.get(url)
    # Check if the request was successful
    if response.status_code == 200:
    # Decode the content of the response as text
        csv_content = response.content.decode("utf-8")
    # Create a pandas DataFrame from the CSV data
        df = pd.read_csv(io.StringIO(csv_content))
    else:
        print("Failed to download the CSV file")
    df['wiki_language'] = df['wiki'].apply(lambda x: x.split('.')[0].split('//')[1])
    df = df[~df['article_title'].str.contains(":")] #delete all rows that are edits in User pages or talk pages
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    return df


def process_dataframe_in_batches(df, batch_size, function_to_apply):
    new_df_list = []

    for i in range(0, len(df), batch_size):
        batch_df = df.iloc[i:i + batch_size]

        if len(batch_df) == 0:
            continue

        batch_df = batch_df.dropna(subset=function_to_apply['columns_to_drop_na'])

        if len(batch_df) == 0:
            continue

        print(f"Processing batch {i + 1}-{i + len(batch_df)} out of {len(df)}")

        try:
            # Apply the function to the entire DataFrame
            result = batch_df.apply(function_to_apply['function'], axis=1)
            new_df_list.append(pd.DataFrame(result.tolist(), columns=function_to_apply['result_columns']))
        except Exception as e:
            print(f"Error during processing: {e}")

        print(f"Processed {i + 1}-{i + len(batch_df)} rows")

    if new_df_list:
        new_df = pd.concat(new_df_list, ignore_index=True, sort=False, axis=0)
        df = pd.concat([df, new_df], axis=1)

    print("Processing complete.")
    return df

def get_liftwing_topics(page_title, lang):
    inference_url = 'https://api.wikimedia.org/service/lw/inference/v1/models/outlink-topic-model:predict'

    headers = {}
    data = {"page_title": page_title, "lang": lang}

    try:
        response = requests.post(inference_url, headers=headers, data=json.dumps(data))

        if response.status_code == 200:
            result = response.json()
            prediction = result.get('prediction', {})
            topics = prediction.get('results', [])
            return topics
        else:
            print(f"Failed to get LiftWing topics for '{page_title}' - HTTP Status Code: {response.status_code}")
            print("Response content:", response.text)
            return None
    except Exception as e:
        print(f"Error: {e}")
        return None


# Function to get the highest-rated topic and its score
def get_highest_rated_topic(row):
    article_title = row['article_title']
    wiki_language = row['wiki_language']
    
    topics = get_liftwing_topics(article_title, wiki_language)

    if topics:
        sorted_topics = sorted(topics, key=lambda x: x['score'], reverse=True)
        highest_topic = sorted_topics[0]['topic']
        highest_score = sorted_topics[0]['score']
        return highest_topic, highest_score
    else:
        return None, None

def get_wikidata_id(article_title, language):
    try:
        # Replace spaces with underscores in the article title
        formatted_title = article_title.replace(" ", "_")

        # Construct the sites parameter using language
        sites_parameter = f"{language}wiki"

        # Wikidata API URL to get entity information
        wikidata_api_url = f"https://www.wikidata.org/w/api.php?action=wbgetentities&sites={sites_parameter}&titles={formatted_title}&languages={language}&format=json"

        # Send a GET request to the Wikidata API
        response = requests.get(wikidata_api_url)

        # Parse the response as JSON
        data = response.json()

        # Extract Wikidata ID (QID) from the JSON response
        entities = data.get("entities", {})
        qid = next(iter(entities), None)

        # Check if QID is not None before returning
        if qid is not None:
            return qid
        else:
            print(f"No Wikidata ID found for '{article_title}' in {language} Wikipedia")
            return None
    except Exception as e:
        print(f"Error retrieving Wikidata ID for '{article_title}' in {language} Wikipedia: {e}")
        return None

def get_wikidata_id_row(row):
    article_title = row['article_title']
    language = row['wiki_language']
    return get_wikidata_id(article_title, language)

def get_instance_of_labels_row(row):
    wikidata_id = row['wikidata_id']
    language = row['wiki_language']
    return get_instance_of_labels(wikidata_id, language)

def get_instance_of_labels(wikidata_id, language):
    try:
        query_string = f"""
            SELECT ?instance_ofLabel
            WHERE {{
                wd:{wikidata_id} wdt:P31 ?instance_of.
                SERVICE wikibase:label {{ bd:serviceParam wikibase:language "{language}" }}
            }}
        """
        res = return_sparql_query_results(query_string)

        labels = []
        if "results" in res and "bindings" in res["results"]:
            bindings = res["results"]["bindings"]
            labels = [binding["instance_ofLabel"]["value"] for binding in bindings]

        return '|'.join(labels)
    except Exception as e:
        print(f"Error in SPARQL query: {e}")
        return None
