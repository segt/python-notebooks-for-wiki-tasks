# Python notebooks for Wiki tasks
## Description
In this folder you'll find several Python Notebooks that can serve for different Wikitasks. For example, [this code]("https://gitlab.wikimedia.org/segt/python-notebooks-for-wiki-tasks/-/blob/main/get-bytes-for-wikipedia-articles.ipynb?ref_type=heads") takes a database with a list of Wikipedia articles and the language of the Wikipedia, and returns the size of the Wikipedia article.

## Support
I'm not currently maintaining this repository as a main task, but feel free to book one of my Open Hours if you want any assistance running the code and I might be able to help!

## Contributing
Have ideas for improvement? Do reach out! Either here or through my email: silviaegt at wikimedia dot org.

## Authors and acknowledgment
Silvia Gutiérrez is the author of the code here, but would like to add special thanks to Morten Warncke-Wang from WMF who was key in understanding some aspects of the API.
## License
CC BY 4.0